unit UniqueCatalogsU;

{$mode objfpc}{$H+}

interface

uses
   Classes
  ,SysUtils
  ,IniFiles
  ;

type

  { TUniqueCatalogs }

  TUniqueCatalogs = class
  private
    FIniFile: TIniFile;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(const AKey: string; ACatalogName: string = 'default');
    function Exists(const AKey: string; ACatalogName: string = 'default'): Boolean;
  end;

var
  UniqueCatalogs: TUniqueCatalogs;

implementation

{ TUniqueCatalogs }

constructor TUniqueCatalogs.Create;
begin
  FIniFile := TIniFile.Create('UniqueCatalog.ini');
end;

destructor TUniqueCatalogs.Destroy;
begin
  FIniFile.Free;
  inherited Destroy;
end;

procedure TUniqueCatalogs.Add(const AKey: string; ACatalogName: string);
begin
  FIniFile.WriteString(ACatalogName, AKey, '1');
end;

function TUniqueCatalogs.Exists(const AKey: string; ACatalogName: string
  ): Boolean;
begin
  Result := FIniFile.ReadString(ACatalogName, AKey, '0') = '1';
end;

initialization
  UniqueCatalogs.Create;

finalization
  UniqueCatalogs.Destroy;

end.

